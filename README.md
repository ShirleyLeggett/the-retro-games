# The Retro Games 

I know you would like to know how I did it. I finally see the masterpiece of my art. I created the different version of retro games I had in my workshop and I would like to share some of the information to you.

Look how my ideas come up to this quaint and creative art – this is sort of [wall fountains](https://www.soothingcompany.com/water-fountains/wall-fountains.html)  everywhere. The first retro game I created is the Asteroids floating in the space, and the second one is the Luna lander where I used to think I’m the one in the moon - great! The third photo is the Space invader and the last one is “snake”. 



![Screenshot of Asteroids](asteroids/screenshot.png)

![Screenshot of Lunar Lander](lunar-lander/screenshot.png)

![Screenshot of Space Invaders](space-invaders/screenshot.png)

![Screenshot of Snake](snake/screenshot.png)